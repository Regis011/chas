<?php
/**
 * Template Name: Follows Page Template
 *
 * Follows Page Template.
 *
 */

get_header();

$follows = get_user_meta( get_current_user_id(),  'follows', true );

if($follows == null){
  $follows = 1;
}

$args = array(
  'post_type' => 'chas_post',
  'posts_per_page' => -1,
  'author__in' => $follows,
);

$the_query = new WP_Query( $args );

?>

	<div id="primary" class="content-area">
		<main id="main" class="site-main" role="main">

		<?php if ( $the_query->have_posts() ) : ?>

			<header class="page-header">
				<?php
					the_title( '<h1 class="page-title">', '</h1>' );
					the_archive_description( '<div class="taxonomy-description">', '</div>' );
				?>
			</header><!-- .page-header -->

			<?php /* Start the Loop */ ?>
			<?php while ( $the_query->have_posts() ) : $the_query->the_post(); ?>

				<?php
					get_template_part( 'template-parts/content', 'archive' );
				?>

			<?php endwhile; ?>

			<?php the_posts_navigation(); ?>

		<?php else : ?>

			<?php get_template_part( 'template-parts/content', 'none_followers' ); ?>

		<?php endif; ?>

		</main><!-- #main -->
	</div><!-- #primary -->

<?php get_sidebar(); ?>
<?php get_footer(); ?>
