<?php
// Setup Archive page to authors
add_action( 'pre_get_posts', function ( $q ) {

    if( !is_admin() && $q->is_main_query() && $q->is_author() ) {
        $q->set( 'posts_per_page', 100 );
        $q->set( 'post_type', 'chas_post' );
    }

});

// Ajax Function
add_action( 'wp_ajax_nopriv_chas_ajax_folow', 'chas_ajax_folow' );
add_action( 'wp_ajax_chas_ajax_folow', 'chas_ajax_folow' );

function chas_ajax_folow() {
    $folow = $_POST['folow'];
    $post_id = $_POST['postid'];
    $author_id = $_POST['authorid'];
    $curent_user_id = $_POST['curentuserid'];

    echo $folow . ' folow, ';
    // echo $post_id . ' post ID, ';
    echo $author_id . ' author ID, ';
    // echo $curent_user_id . ' curent user ID, ';

    if( $folow === 'unchecked') {
      /*
      * Unset New Folows
      */
      $new_follows = get_user_meta( $curent_user_id,  'follows', true );
      $key_follows = array_search($author_id, $new_follows);

      if( $key_follows !== false) {
        unset($new_follows[$key_follows]);
      }

      update_user_meta( $curent_user_id, 'follows', $new_follows );

      /*
      * Unset New Folowers
      */
      $new_followers = get_user_meta( $author_id,  'followers', true );
      $key_followers = array_search($curent_user_id, $new_followers);

      if( $key_followers !== false) {
        unset($new_followers[$key_followers]);
      }

      update_user_meta( $author_id, 'followers', $new_followers );

      print_r(get_user_meta( $author_id,  'followers', true ));


    }
    print_r(get_user_meta( $curent_user_id,  'follows', true ));
    print_r(get_user_meta( $author_id,  'followers', true ));

    if( $folow === 'checked'){

      /*
      * New Folows
      */
      $new_follows = get_user_meta( $curent_user_id,  'follows', true );
      if (!is_array($new_follows)){
        $new_follows = array();
      }

      if (in_array($author_id, $new_follows)){
        return false;
      }

      $new_follows[] = $author_id;

      // will return false if the previous value is the same as $new_follows
      if(!empty($new_follows)){
        update_user_meta( $curent_user_id, 'follows', $new_follows );
      }

      /*
      * New Folowers
      */
      $new_followers = get_user_meta( $author_id, 'followers', true );
      if (!is_array($new_follows)){
        $new_followers = array();
      }

      if (in_array($curent_user_id, $new_followers)){
        return false;
      }

      $new_followers[] = $curent_user_id;

      if(!empty($new_followers)){
         update_user_meta( $author_id, 'followers', $new_followers );
      }

    }
}
