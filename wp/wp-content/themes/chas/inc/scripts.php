<?php

/**
 * Enqueue scripts and styles.
 */
function np011_scripts() {

	wp_enqueue_style( 'np011-style', get_stylesheet_directory_uri() . '/style.min.css', array(), '1.0.0' );

	wp_enqueue_script( 'np011-theme', get_template_directory_uri() . '/js/dist/app.min.js', array( 'jquery' ), '1.0.0', true );

	wp_enqueue_script( 'np011-plugin-js', get_template_directory_uri() . '/js/dist/plugins.min.js', array( 'jquery' ), '1.0.0', true );

	//Localize
	global $wp_query;
	wp_localize_script('np011-theme', 'chas', array(
		'ajaxurl' => admin_url('admin-ajax.php'),
		'qvars' => json_encode($wp_query->query),
		'postID' => get_the_ID(),
		'curentUserID' => get_current_user_id(),
	));

	if ( is_singular() && comments_open() && get_option( 'thread_comments' ) ) {
		wp_enqueue_script( 'comment-reply' );
	}
}
add_action( 'wp_enqueue_scripts', 'np011_scripts' );
