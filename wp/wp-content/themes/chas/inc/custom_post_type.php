<?php
// Register Custom Post Type
function chas_custom_post_type() {

  $publicly_queryable = false;
  if ( is_user_logged_in() )
    $publicly_queryable = true;

	$labels = array(
		'name'                  => _x( 'Chas Inlägg', 'Post Type General Name', 'np011' ),
		'singular_name'         => _x( 'Chas Inlägg', 'Post Type Singular Name', 'np011' ),
		'menu_name'             => __( 'Chas inlägg', 'np011' ),
		'name_admin_bar'        => __( 'Chas inlägg', 'np011' )
	);
	$capabilities = array(
		'edit_post'             => 'chas_edit_post',
		'read_post'             => 'chas_read_post',
		'delete_post'           => 'chas_delete_post',
		'edit_posts'            => 'chas_edit_posts',
		'edit_others_posts'     => 'chas_edit_others_posts',
		'publish_posts'         => 'chas_publish_posts',
		'read_private_posts'    => 'chas_read_private_posts',
	);
	$args = array(
		'label'                 => __( 'Chas Inlägg', 'np011' ),
		'description'           => __( 'Post Type Description', 'np011' ),
		'labels'                => $labels,
		'supports'              => array( 'title', 'editor', 'excerpt', 'author', 'thumbnail', ),
		'taxonomies'            => array( 'chas_taxonomy' ),
		'hierarchical'          => false,
		'public'                => true,
		'show_ui'               => true,
		'show_in_menu'          => true,
		'menu_position'         => 5,
		'menu_icon'             => 'dashicons-format-aside',
		'show_in_admin_bar'     => true,
		'show_in_nav_menus'     => true,
		'can_export'            => true,
		'has_archive'           => true,
		'exclude_from_search'   => false,
		'publicly_queryable'    => $publicly_queryable,
		'capabilities'          => $capabilities,
		'show_in_rest'          => false,
	);
	register_post_type( 'chas_post', $args );

}
add_action( 'init', 'chas_custom_post_type', 0 );

// Register Custom Taxonomy
function chas_taxonomy() {

	$labels = array(
		'name'                       => _x( 'Chas Taxonomies', 'Taxonomy General Name', 'np011' ),
		'singular_name'              => _x( 'Chas Taxonomy', 'Taxonomy Singular Name', 'np011' ),
		'menu_name'                  => __( 'Chas Taxonomy', 'np011' )
	);
	$capabilities = array(
		'manage_terms'               => 'chas_manage_terms',
		'edit_terms'                 => 'chas_edit_terms',
		'delete_terms'               => 'chas_delete_terms',
		'assign_terms'               => 'chas_edit_posts',
	);
	$args = array(
		'labels'                     => $labels,
		'hierarchical'               => false,
		'public'                     => true,
		'show_ui'                    => true,
		'show_admin_column'          => true,
		'show_in_nav_menus'          => true,
		'show_tagcloud'              => true,
		'capabilities'               => $capabilities,
    'map_meta_cap'               => true
	);
	register_taxonomy( 'chas_taxonomy', array( 'chas_post' ), $args );

}
add_action( 'init', 'chas_taxonomy', 0 );


$caps = 'administrator';
do_action('theme_chas_caps', $caps);
