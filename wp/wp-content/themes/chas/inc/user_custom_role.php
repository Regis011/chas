<?php
// Add a custom user role Chas
$result = add_role( 'chas', __(
'Chas' ),
  array(
  'read' => false,
  'level_0' => true,
  'chas_edit_post' => true,
  'chas_read_post' => true,
  'chas_delete_post' => true,
  'chas_edit_posts' => true,
  'chas_edit_others_posts'=> false,
  'chas_publish_posts' => true,
  'chas_read_private_posts' => true,
  'chas_manage_categories' => true,
  'chas_manage_terms' => true,
  'chas_edit_terms' => true,
  'chas_delete_terms' => true
  )
);

$caps = 'chas';
do_action('theme_chas_caps', $caps);
