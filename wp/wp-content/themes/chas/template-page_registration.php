<?php
/**
 * Template Name: Registration Page
 *
 * Registration Page Template.
 *
 */

get_header(); ?>

	<!-- section -->
    <section class="registration">

      <div class="col-md-8 col-md-offset-2">

        <?php chas_registration(); ?>

      </div>

	</section>
	<!-- /section -->

<?php get_footer(); ?>
