(function(root, document, $) {
	'use strict';
	var app = {
		init : function() {
			this.win = $(window);
			this.doc = $(document);
			this.body = $('body');

			for (var name in this) {
				if (typeof(this[name].init) === 'function') {
					this[name].init();
				}
			}

		},
		test_func : {
			init : function() {
				console.log('Test init!');
				console.log(chas.ajaxurl);
			}
		},
		folow_checkbox: {
			init: function(){
				$(document).on('click', '#folow', function(){
					console.log('click');
					var t = $(this);
					var folow = '';
					var authorID = t.data('author');
					if(t.is(':checked')){
						folow = 'checked';
					} else {
						folow = 'unchecked';
					}

					$.ajax({
						url: chas.ajaxurl,
						type: 'post',
						data: {
							action: 'chas_ajax_folow',
							folow: folow,
							authorid: authorID,
							postid: chas.postID,
							curentuserid: chas.curentUserID,
						},
						success: function( res ) {
							console.log(res);
						}
					});

				});

			}
		}
	};
	// publish
	root.app = app;
	// init
	$( root ).ready(function() {
		app.init();
	});
})(window, document, jQuery);
