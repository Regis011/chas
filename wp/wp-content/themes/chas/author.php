<?php
/**
 * The template for displaying author pages.
 *
 *
 * @package np011
 */

get_header();

$follows = get_user_meta( get_current_user_id(),  'follows', true );
$author = get_user_by( 'slug', get_query_var( 'author_name' ) );
$author_id = $author->ID;
?>

	<div id="primary" class="content-area">
		<main id="main" class="site-main" role="main">

		<?php if ( have_posts() ) : ?>

			<header class="page-header">
				<?php

					the_archive_title( '<h1 class="page-title">', '</h1>' );
          echo '<label>Folja</label>';
          echo '<input type="checkbox" id="folow" data-author="' . $author_id . '" value="folow"';
					if(is_array($follows)){
						if(in_array($author_id, $follows)){
							echo 'checked';
						}
					}
					echo ' >';
					the_archive_description( '<div class="taxonomy-description">', '</div>' );

				?>
			</header><!-- .page-header -->

			<?php /* Start the Loop */ ?>
			<?php while ( have_posts() ) : the_post(); ?>

				<?php
					/* Include the Post-Format-specific template for the content.
					 * If you want to override this in a child theme, then include a file
					 * called content-___.php (where ___ is the Post Format name) and that will be used instead.
					 */
					get_template_part( 'template-parts/content', 'archive' );
				?>

			<?php endwhile; ?>

			<?php the_posts_navigation(); ?>

		<?php else : ?>

			<?php get_template_part( 'template-parts/content', 'none' ); ?>

		<?php endif; ?>

		</main><!-- #main -->
	</div><!-- #primary -->

<?php get_sidebar(); ?>
<?php get_footer(); ?>
