<?php
/**
 * np011 functions and definitions
 *
 * @package np011
 */

/**
 * Set the content width based on the theme's design and stylesheet.
 */
if ( ! isset( $content_width ) ) {
	$content_width = 640; /* pixels */
}

if ( ! function_exists( 'np011_setup' ) ) :
/**
 * Sets up theme defaults and registers support for various WordPress features.
 *
 * Note that this function is hooked into the after_setup_theme hook, which
 * runs before the init hook. The init hook is too late for some features, such
 * as indicating support for post thumbnails.
 */
function np011_setup() {

	/*
	 * Make theme available for translation.
	 * Translations can be filed in the /languages/ directory.
	 * If you're building a theme based on np011, use a find and replace
	 * to change 'np011' to the name of your theme in all the template files
	 */
	load_theme_textdomain( 'np011', get_template_directory() . '/languages' );

	// Add default posts and comments RSS feed links to head.
	add_theme_support( 'automatic-feed-links' );

	/*
	 * Let WordPress manage the document title.
	 * By adding theme support, we declare that this theme does not use a
	 * hard-coded <title> tag in the document head, and expect WordPress to
	 * provide it for us.
	 */
	add_theme_support( 'title-tag' );

	/*
	 * Enable support for Post Thumbnails on posts and pages.
	 *
	 * @link http://codex.wordpress.org/Function_Reference/add_theme_support#Post_Thumbnails
	 */
	//add_theme_support( 'post-thumbnails' );

	// This theme uses wp_nav_menu() in one location.
	register_nav_menus( array(
		'primary' => esc_html__( 'Primary Menu', 'np011' ),
	) );

	/*
	 * Switch default core markup for search form, comment form, and comments
	 * to output valid HTML5.
	 */
	add_theme_support( 'html5', array(
		'search-form', 'comment-form', 'comment-list', 'gallery', 'caption',
	) );

	/*
	 * Enable support for Post Formats.
	 * See http://codex.wordpress.org/Post_Formats
	 */
	add_theme_support( 'post-formats', array(
		'aside', 'image', 'video', 'quote', 'link',
	) );

	// Set up the WordPress core custom background feature.
	add_theme_support( 'custom-background', apply_filters( 'np011_custom_background_args', array(
		'default-color' => 'ffffff',
		'default-image' => '',
	) ) );
}
endif; // np011_setup
add_action( 'after_setup_theme', 'np011_setup' );

/**
 * Register widget area.
 *
 * @link http://codex.wordpress.org/Function_Reference/register_sidebar
 */
function np011_widgets_init() {

    register_sidebar( array(
		'name'          => esc_html__( 'Sidebar', 'np011' ),
		'id'            => 'sidebar-1',
		'description'   => '',
		'before_widget' => '<aside id="%1$s" class="widget %2$s">',
		'after_widget'  => '</aside>',
		'before_title'  => '<h1 class="widget-title">',
		'after_title'   => '</h1>',
	) );
}
add_action( 'widgets_init', 'np011_widgets_init' );

// Custom Chas Copabilities
function add_theme_chas_caps($caps) {
    // gets the role
    $admins = get_role( $caps );

    $admins->add_cap( 'chas_edit_post' );
    $admins->add_cap( 'chas_read_post' );
    $admins->add_cap( 'chas_delete_post' );
    $admins->add_cap( 'chas_edit_posts' );
    $admins->add_cap( 'chas_edit_others_posts' );
    $admins->add_cap( 'chas_publish_posts' );
    $admins->add_cap( 'chas_read_private_posts' );
    $admins->add_cap( 'chas_manage_terms' );
    $admins->add_cap( 'chas_edit_terms' );
    $admins->add_cap( 'chas_delete_terms' );

}
add_action( 'theme_chas_caps', 'add_theme_chas_caps');

function chas_nav_menu( $args = '' ) {
	if( is_user_logged_in() ) {
		$args['menu'] = 'logged-in';
	} else {
		$args['menu'] = 'logged-out';
	}
		return $args;
	}

add_filter( 'wp_nav_menu_args', 'chas_nav_menu' );
/**
 * Enqueue scripts and styles.
 */
require get_template_directory() . '/inc/scripts.php';

/**
 * Load Bootstrap Menu.
 */
require get_template_directory() . '/inc/bootstrap-walker.php';

/**
 * Custom template tags for this theme.
 */
require get_template_directory() . '/inc/template-tags.php';

/**
 * Custom functions that act independently of the theme templates.
 */
require get_template_directory() . '/inc/extras.php';

/**
 * Theme Options.
 */
require get_template_directory() . '/inc/np011_theme_options.php';

/**
 * Load Jetpack compatibility file.
 */
require get_template_directory() . '/inc/jetpack.php';

/**
 * User Custom Role.
 */
require get_template_directory() . '/inc/user_custom_role.php';

/**
 * Custom Post Type.
 */
require get_template_directory() . '/inc/custom_post_type.php';

/**
 * Custom Registration.
 */
require get_template_directory() . '/inc/custom-registration.php';

/**
 * Folow Functions.
 */
require get_template_directory() . '/inc/folow_function.php';


/* Main redirection of the default login page */
function redirect_login_page() {
	$login_page  = home_url('/logga-in/');
	$page_viewed = basename($_SERVER['REQUEST_URI']);

	if($page_viewed == "wp-login.php" && $_SERVER['REQUEST_METHOD'] == 'GET') {
		wp_redirect($login_page);
		exit;
	}
}
add_action('init','redirect_login_page');

/* Where to go if a login failed */
function custom_login_failed() {
	$login_page  = home_url('/logga-in/');
	wp_redirect($login_page . '?login=failed');
	exit;
}
add_action('wp_login_failed', 'custom_login_failed');

/* Where to go if any of the fields were empty */
function verify_user_pass($user, $username, $password) {
	$login_page  = home_url('/logga-in/');
	if($username == "" || $password == "") {
		wp_redirect($login_page . "?login=empty");
		exit;
	}
}
add_filter('authenticate', 'verify_user_pass', 1, 3);

/* What to do on logout */
function logout_redirect() {
	$login_page  = home_url('/logga-in/');
	wp_redirect($login_page . "?login=false");
	exit;
}
add_action('wp_logout','logout_redirect');

// Login redirect
function admin_default_page() {
	wp_redirect( '/chas_post' );
	    exit;
}
add_filter('login_redirect', 'admin_default_page');

// Don't show followers page of unser not logetin
function redirect_non_logged_users_to_specific_page() {

	if ( !is_user_logged_in() && is_page('foljer') && $_SERVER['PHP_SELF'] != '/wp-admin/admin-ajax.php' ) {

	wp_redirect( '/' );
	    exit;
	}

}
add_action( 'admin_init', 'redirect_non_logged_users_to_specific_page' );
