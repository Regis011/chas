<?php
/**
 * Template Name: Login Page
 *
 * Login Page Template.
 *
 */

get_header(); ?>

	<!-- section -->
    <section class="login">
      <div class="col-md-8 col-md-offset-2">

        <?php
            global $user_login;

            // In case of a login error.
            if ( isset( $_GET['login'] ) && $_GET['login'] == 'failed' ) : ?>
    	            <div class="col-md-12">
    		            <p><?php _e( 'FAILED: Try again!', 'np011' ); ?></p>
    	            </div>
            <?php
                endif;

            // If user is already logged in.
            if ( is_user_logged_in() ) : ?>

                <div class="logout">

                    <?php
                        _e( 'Hej!', 'np011' );
                        echo $user_login;
                    ?>

                    </br>

                    <?php _e( 'You are already logged in.', 'np011' ); ?>

                </div>

                <a id="wp-submit" href="<?php echo wp_logout_url(); ?>" title="Logout">
                    <?php _e( 'Logout', 'np011' ); ?>
                </a>

            <?php
                // If user is not logged in.
                else:

                    // Login form arguments.
                    $args = array(
                        'echo'           => true,
                        'redirect'       => home_url( '/wp-admin/' ),
                        'form_id'        => 'loginform',
                        'label_username' => __( 'Username' ),
                        'label_password' => __( 'Password' ),
                        'label_remember' => __( 'Remember Me' ),
                        'label_log_in'   => __( 'Log In' ),
                        'id_username'    => 'user_login',
                        'id_password'    => 'user_pass',
                        'id_remember'    => 'rememberme',
                        'id_submit'      => 'wp-submit',
                        'remember'       => true,
                        'value_username' => NULL,
                        'value_remember' => true
                    );

                    // Calling the login form.
                    wp_login_form( $args );

                endif;

        ?>

      </div>

	</section>
	<!-- /section -->

<?php get_footer(); ?>
