<?php
/**
 * The template part for displaying a message that posts cannot be found.
 *
 * Learn more: http://codex.wordpress.org/Template_Hierarchy
 *
 * @package np011
 */
?>

<section class="no-results not-found">
	<header class="page-header">
		<h1 class="page-title"><?php esc_html_e( 'Du följer inte någon.', 'np011' ); ?></h1>

	</header><!-- .page-header -->

	<div class="page-content">

			<p>
        <a href="/chas_post"><?php _e('Gå till Chas inlägg', 'np011'); ?></a>
        <?php esc_html_e( 'och välja autor som du vill följa.', 'np011' ); ?></p>

	</div><!-- .page-content -->
</section><!-- .no-results -->
