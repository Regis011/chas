<?php
/**
 * Baskonfiguration för WordPress.
 *
 * Denna fil används av wp-config.php-genereringsskript under installationen.
 * Du behöver inte använda webbplatsen, du kan kopiera denna fil direkt till
 * "wp-config.php" och fylla i värdena.
 *
 * Denna fil innehåller följande konfigurationer:
 *
 * * Inställningar för MySQL
 * * Säkerhetsnycklar
 * * Tabellprefix för databas
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL-inställningar - MySQL-uppgifter får du från ditt webbhotell ** //
/** Namnet på databasen du vill använda för WordPress */
define('DB_NAME', 'chas');

/** MySQL-databasens användarnamn */
define('DB_USER', 'root');

/** MySQL-databasens lösenord */
define('DB_PASSWORD', 'root');

/** MySQL-server */
define('DB_HOST', 'localhost');

/** Teckenkodning för tabellerna i databasen. */
define('DB_CHARSET', 'utf8mb4');

/** Kollationeringstyp för databasen. Ändra inte om du är osäker. */
define('DB_COLLATE', '');

/**#@+
 * Unika autentiseringsnycklar och salter.
 *
 * Ändra dessa till unika fraser!
 * Du kan generera nycklar med {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * Du kan när som helst ändra dessa nycklar för att göra aktiva cookies obrukbara, vilket tvingar alla användare att logga in på nytt.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         'AKLRi_oFod,D9/-U,zmbF+5k=.*VW,J|$%^wX35+4XNQ>`#^-o(hQa@$SGhwSUE8');
define('SECURE_AUTH_KEY',  '$7kPLZL>t> 3rX>jwx:kc:Q;U?|ye]Syo-^#/HI?Y@e=NQ Gg8LCVlyWtd^qYFgK');
define('LOGGED_IN_KEY',    '7k$ QzT:/16%z. *yN164]~i[d*1m|i}/7.=!|!&a;3AZrb&xi|.}2ag*j4+QrP-');
define('NONCE_KEY',        'QV?*?9d13EfAmVl<]B?fol&gM/.3IS0:@*8WhQhi!l-@fF|9mAJ)HU>!NwfgT70x');
define('AUTH_SALT',        'a00fe.@Zmh_$+@XxKD>Er Y!2O2h*2IdBreuB2i@[E1PcoOZs5crq}a8K4K;_fEx');
define('SECURE_AUTH_SALT', 'y9CJsA6P@N8yBb#pE>z?{F;L<! T`s%N~ETaONk|Z)oeRM?#6>-uhK!Es5E9T,sb');
define('LOGGED_IN_SALT',   '~/x{IshNL]BpXDP:3spR7dGPC~!NM~M7{OR@gZ:^!8?ddlyBl$&]-/9,N|ZDXOcw');
define('NONCE_SALT',       '0g_Mf(lB&7vWs(4n .RVXTfp@}L$um*1[G|Xq8D}$v3-oJ&:`ONe-Y<bXh)DI!1I');

/**#@-*/

/**
 * Tabellprefix för WordPress Databasen.
 *
 * Du kan ha flera installationer i samma databas om du ger varje installation ett unikt
 * prefix. Endast siffror, bokstäver och understreck!
 */
$table_prefix  = 'chas_wp_';

/**
 * För utvecklare: WordPress felsökningsläge.
 *
 * Ändra detta till true för att aktivera meddelanden under utveckling.
 * Det är rekommderat att man som tilläggsskapare och temaskapare använder WP_DEBUG
 * i sin utvecklingsmiljö.
 *
 * För information om andra konstanter som kan användas för felsökning,
 * se dokumentationen.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', true);

/* Det var allt, sluta redigera här! Blogga på. */

/** Absoluta sökväg till WordPress-katalogen. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Anger WordPress-värden och inkluderade filer. */
require_once(ABSPATH . 'wp-settings.php');
