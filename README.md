# README - Chas  Open souce development

Det är en Wordpree APP. Alla wp filer ligger under wp map och databas för detta ligger under db map. 

För detta projekt jag har använt min starter teman som jag har klonad fråm github.
Mer info om teman finns under tema mapen i readme.md fil. (root/wp/wp-content/themes/chas)
Länk till teman: [NP011 theme](https://github.com/Regis011/np011)

### Beskrivnin om app-en

När man går som loggat in användare då man kommer att visas en "post type" som heter "Chas" och som visas inlägg och författarna som man kan följa eller sluta följa.  
Att följa en författare man måste gå till författare arkiv sida. (man kan klicka på "by författarenamn") På författare arkiv sida finns en "checkbox" där man kan krisa detta om vill följa detta författare eller krysa av om vill sluta följa detta författare. 
Alla författarna som man följer visas på sidan "Följer". På samma sätt via länker man kunna sluta följa författarna. 

## WordPress

### WP - admin
anv: Vlada

lösen: Stockholm

#### Test Användare konto
anv1: Mark

anv2: Anna

lösen(båda): Stockohlm

### Lokal Databas
root/db/

### Config fil
root/wp/wp-congig.php

### Jobba vidare med teman, kika i teman readme.md fil
root/wp/wp-content/theme/chas/readme.md

## Plugins

Jag har använt bara en tillägg. FakerPress är ett rent sätt att generera falska data till WordPress-installation. Tillägg har bra betygg 
och är säkert för installation och användning.  
[Fakepress](https://wordpress.org/plugins/fakerpress/)

##  Vad har jag gjort? 

Jag har byggt mina egna lösning att får detta projekt att fungera. (Inga externa tillägg) 

- Custom Post Type - archive template - theme_root/archive-chas_post.php

- Custom Taxonomy - theme_root/inc/custom_post_type.php

- Custom Users Role - theme_root/inc/user_custom_role.php

- Custom compability

- Custom Archive Template

- Custom menu to loged-in/out users

- Registration form theme_root/inc/custom-registration.php and template template-page_registration.php

- Login form - theme_root/template-page_login.php redirect function in function.php

- Twitter funktion - theme_root/inc/folow-function.php

- Twitter function - theme_root/template-folows.php
